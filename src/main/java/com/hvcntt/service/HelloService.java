package com.hvcntt.service;

import org.springframework.stereotype.Service;

@Service
public class HelloService {

	public void say() {
		System.out.println("spring hello world");
	}
	
}
