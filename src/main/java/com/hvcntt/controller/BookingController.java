package com.hvcntt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hvcntt.service.BookingService;
import com.hvcntt.service.HelloService;

@Controller
public class BookingController {

	@Autowired
	private BookingService bookingService;
	
	@RequestMapping("/")
	public String index() {
		bookingService.say();
		return "index";
	}
	@RequestMapping(value = "/booking", method = RequestMethod.GET)
	public String booking() {
		
		System.out.println("call when click hello a href");
		return "booking";
	}
}