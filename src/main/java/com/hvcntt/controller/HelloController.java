package com.hvcntt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hvcntt.service.HelloService;

@Controller
public class HelloController {

	
	@Autowired
	private HelloService helloService;
	
	@RequestMapping("/")
	public String index() {
		helloService.say();
		
		return "hello";
	}
	
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public String welcome() {
		
		System.out.println(" welcome page call when click hello a href");
		return "booking";
	}
	
	

}
