package com.hvcntt.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
	public class ViewController {
	    
	    @RequestMapping(value = "/template.htm", method = RequestMethod.GET)
	    public String init() {
	        return "template";
	    }
	    
	    @RequestMapping(value = "/home.htm", method = RequestMethod.GET)
	    public String goToPage1() {
	        return "home";
	    }
	 
	
}
